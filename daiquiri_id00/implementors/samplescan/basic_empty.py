#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from marshmallow import fields, validate

from daiquiri.core.schema.validators import OneOf
from daiquiri.core.components import (
    ComponentActor,
    ComponentActorSchema,
)


class Basic_EmptySchema(ComponentActorSchema):
    sampleid = fields.Int(required=True)
    motor_start = fields.Float(required=True, title="Start Position")
    motor_end = fields.Float(required=True, title="End Position")
    npoints = fields.Int(required=True, title="No. Points")
    time = fields.Float(
        validate=validate.Range(min=0.1, max=5),
        required=True,
        title="Time(s) per Point",
    )
    detectors = fields.List(
        OneOf(["diode", "simu1", "lima_simulator"]),
        uniqueItems=True,
        minItems=1,
        required=True,
        title="Detectors",
    )
    enqueue = fields.Bool(title="Queue Scan", default=True)

    class Meta:
        uischema = {"sampleid": {"classNames": "hidden-row", "ui:widget": "hidden"}}


class Basic_EmptySchema(ComponentActor):
    schema = Basic_EmptySchema
    name = "Basic_Empty"
    saving_args = {"data_filename": "{sampleid.name}_scan{datacollectionid}"}

    def method(self, **kwargs):
        print("capture params")
        kwargs["before_scan_starts"](self)

        # do something here
        time.sleep(5)

        # update the database with metadata
        kwargs["update_datacollection"](
            self,
            datacollectionnumber=1,
            imagecontainersubpath="1.1/measurement",
            numberofimages=kwargs["npoints"],
            exposuretime=kwargs["time"],
        )
