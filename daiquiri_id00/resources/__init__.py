"""All non-python files used by the project.

When `daiquiri` is looking for a resource, it first tries looking in
this module (path passed in app.py) and then in `daiquiri.resources`.
"""
