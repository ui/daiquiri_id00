# -*- coding: utf-8 -*-

# Allows running the REST server with "python -m daiquiri_daiquiri_id00"
from daiquiri_daiquiri_id00.app import main

main()
